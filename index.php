<?php include('header.php'); 
//load the database configuration file
require base_path('/config/dbConfig.php');
?>
</head>
<body>
	

	<div class="container">
		<h1>Data cleaner</h1>
		<?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Actions
			</div>
			<div class="panel-body">
				<input type="submit" class="btn btn-success" name="import" value="Import" onClick="document.location.href='import.php'">
				
				<input type="submit" class="btn btn-primary" name="clean" value="Clean" onClick="document.location.href='dbfunctions/clean.php'">
				<input type="submit" class="btn btn-primary" name="showdupes" value="Show Dupes" onClick="document.location.href='showdupes.php'">
				<input type="submit" class="btn btn-primary" name="showissues" value="Show Issues" onClick="document.location.href='showissues.php'">
				<input type="submit" class="btn btn-primary" name="shownonutf8" value="Highlight non UTF8" onClick="javascript:utf8validate();">
				
				<span style="float:right;">
					<input type="submit" class="btn btn-primary" name="export" value="Export" onClick="document.location.href='dbfunctions/export.php'">
					<input type="submit" class="btn btn-danger" name="truncate" value="Cleardown" onClick="document.location.href='dbfunctions/truncate.php'">
				</span>
			</div>
		</div>
		<?php $query = $db->query( "SELECT * FROM import ORDER BY id DESC" ); ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<span>Judges List</span>
				<span style="float: right;">Total records: <?php echo $query->num_rows; ?></span>
			</div>
			<div class="panel-body">
				<div id="pleasewait" class="valhide">Please wait while we validate...</div>
				<table class="table table-bordered" id="datatable">
					<thead>
						<tr>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>Email</th>
							<th>Filename</th>
							<th>Actions</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						//get records from database
						
						if ( $query->num_rows > 0 ) {
							while ( $row = $query->fetch_assoc() ) {
								?>
						<tr>
							<td class="result">
								<?php echo $row['firstname']; ?>
							</td>
							<td class="result">
								<?php echo $row['lastname']; ?>
							</td>
							<td class="result">
								<?php echo $row['email']; ?>
							</td>
							<td>
								<?php echo $row['filename']; ?>
							</td>
							<td>
								<?php echo $row['actions']; ?>
							</td>
							<td class="contact-actions">
								<form action='dbfunctions/delete.php' method="post">
									<input type="hidden" name="delid" value="<?php echo $row['ID']; ?>">
									<input type="submit" class="btn btn-primary" name="submit" value="Delete">
								</form>
							
								<form action='update.php' method="post">
									<input type="hidden" name="updid" value="<?php echo $row['ID']; ?>">
									<input type="submit" class="btn btn-primary" name="submit" value="Update">
								</form>
							</td>
						</tr>
						<?php } }else{ ?>
						<tr>
							<td colspan="6">No record(s) found.....</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>

		</div>
<?php include('footer.php');