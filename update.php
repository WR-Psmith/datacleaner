<?php include('header.php') ?>

<body>
	<div class="container">
		<h1>Update Record</h1>
		<?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Actions
			</div>
			<div class="panel-body">
				<input type="submit" class="btn btn-primary" name="back" value="Back" onClick="document.location.href='index.php'">
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Update Record:
			</div>
			<div class="panel-body">
				<?php
					$updid = htmlspecialchars($_POST["updid"]);
					if (isset($updid)) {
						$query = $db->query( "SELECT * FROM import where ID = $updid" );
						if ( $query->num_rows == 1 ) {
							while ( $row = $query->fetch_assoc() ) {
								$first = $row['firstname'];
								$last = $row['lastname'];
								$email = $row['email'];
							}
						}
						else {
							$qstring = '?status=noselect';
						}
					}
					else {
						$qstring = '?status=noselect';
					}
				?>
				<form action="dbfunctions/update.php" method="post" enctype="multipart/form-data" id="importFrm">
					<table class="table table-bordered">
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Email</th>
							<th></th>
						<tr>
							<input type="hidden" name="updid" value="<?php echo $updid; ?>">
							<td><input type="text"  name="ufname" value="<?php echo $first; ?>"></td>
							<td><input type="text" name="ulname" value="<?php echo $last; ?>"></td>
							<td><input type="text" name="uemail" value="<?php echo $email; ?>"></td>
							<td><input type="submit" class="btn btn-primary" name="save" value="Save"></td>
						</tr>
						<tr></tr>
					</table>
					
				</form>
			</div>
		</div>
<?php include('footer.php');