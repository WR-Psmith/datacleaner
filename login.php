<?php include('header.php') ?>
</head>
<body>
	

	<div class="container">
		<h1>Data Cleaner</h1>	
		<div class="panel panel-default">
			<div class="panel-heading">
				<span>Login</span>
			</div>
			<div class="panel-body">
			<form action="" method="Post" class="login">
				<table class="table table-borderless">
					<tr>
						<td>Username:</td>
						<td><input type="text" name="username" /></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type="password" name="password" /></td>
					</tr>	
				</table>
				<div>
					<input type="submit" class="btn btn-primary" name="login" value="Login">
				</div>
			</form>
			</div>
		</div>
<?php include('footer.php');