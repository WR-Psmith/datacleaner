<?php include('header.php'); 
//load the database configuration file
require base_path('/config/dbConfig.php');
?>
</head>

<body>
	<div class="container">
		<h1>Data cleaner - Issues for Manual check</h1>
		<?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Actions
			</div>
			<div class="panel-body">
				<input type="submit" class="btn btn-primary" name="back" value="Back" onClick="document.location.href='index.php'">
				<input type="submit" class="btn btn-primary" name="shownonutf8" value="Highlight non UTF8" onClick="javascript:utf8validate();">
				
				<span style="float:right;">
					<input type="submit" class="btn btn-primary" name="export" value="Export" onClick="document.location.href='dbfunctions/exportissues.php'">
					<input type="submit" class="btn btn-danger" name="deleteissues" value="Delete All" onClick="document.location.href='dbfunctions/deleteissues.php'">
				</span>
			</div>
		</div>
		<?php $query = $db->query( "SELECT * FROM import WHERE issue = 1" ); ?>
		<div class="panel panel-default">
			<div class="panel-heading">
			    Records with possible issues
				<span style="float: right;">Total records: <?php echo $query->num_rows; ?></span>
			</div>
			<div class="panel-body">
				<div id="pleasewait" class="valhide">Please wait while we validate...</div>
				<table class="table table-bordered" id="datatable">
					<thead>
						<tr>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>Email</th>
							<th>Filename</th>
							<th>Detail</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						//get records from database
						
						if ( $query->num_rows > 0 ) {
							while ( $row = $query->fetch_assoc() ) {
								?>
						<tr>
							<td class="result">
								<?php echo $row['firstname']; ?>
							</td>
							<td class="result">
								<?php echo $row['lastname']; ?>
							</td>
							<td class="result">
								<?php echo $row['email']; ?>
							</td>
							<td>
								<?php echo $row['filename']; ?>
							</td>
							<td>
								<?php echo $row['details']; ?>
							</td>
							<td class="contact-actions">
								<form action='dbfunctions/delete.php' method="post">
									<input type="hidden" name="delid" value="<?php echo $row['ID']; ?>">
									<input type="submit" class="btn btn-primary" name="submit" value="Delete">
								</form>
							
								<form action='update.php' method="post">
									<input type="hidden" name="updid" value="<?php echo $row['ID']; ?>">
									<input type="submit" class="btn btn-primary" name="submit" value="Update">
								</form>
							</td>
						</tr>
						<?php } }else{ ?>
						<tr>
							<td colspan="5">No record(s) found.....</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>

		</div>
<?php include('footer.php');