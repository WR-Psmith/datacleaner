<?php
//include database configuration file
include '../config/dbConfig.php';

$qstring = '?status=deleted';

$query = $db->query("TRUNCATE TABLE import;");

//redirect to the previous page
header("Location: ".$_SERVER['HTTP_REFERER'].$qstring);

?>