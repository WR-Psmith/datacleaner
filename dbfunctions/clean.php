<?php
include '../config/dbConfig.php';

/* AUTO FIXES */

// Replace line break, tab or carriage reurns with a space
$autoquery1 = $db->query("
UPDATE import SET firstname = REPLACE(REPLACE(REPLACE(firstname, CHAR(10), ' '),CHAR(9), ' '),CHAR(13), ' ');" );
$autoquery2 = $db->query("
UPDATE import SET lastname = REPLACE(REPLACE(REPLACE(lastname, CHAR(10), ' '),CHAR(9), ' '),CHAR(13), ' ');" );
$autoquery3 = $db->query("
UPDATE import SET email = REPLACE(REPLACE(REPLACE(email, CHAR(10), ' '),CHAR(9), ' '),CHAR(13), ' ');" );

// Clear leading / trailing spaces
$autoquery9 = $db->query("
UPDATE import SET email = TRIM(both ' ' FROM email), firstname = TRIM(both ' ' FROM firstname), lastname = TRIM(both ' ' FROM lastname), actions = CONCAT(actions, 'Leading and trailing spaces removed, ') WHERE (email like '% ' or email like ' %') OR (firstname like '% ' or firstname like ' %') OR (lastname like '% ' or lastname like ' %');" );

// Correct case
$autoquery4 = $db->query("
UPDATE import set firstname = LOWER(firstname);" );
$autoquery5 = $db->query("
UPDATE import set firstname = CONCAT(UCASE(SUBSTRING(firstname, 1, 1)),SUBSTRING(firstname, 2));" );
$autoquery6 = $db->query("
UPDATE import set lastname = LOWER(lastname);" );
$autoquery7 = $db->query("
UPDATE import set lastname = CONCAT(UCASE(SUBSTRING(lastname, 1, 1)),SUBSTRING(lastname, 2));" );
$autoquery8 = $db->query("
UPDATE import set email = LOWER(email);" );

/* EXCEPTIONS */

// Multiple inputs in single field
$exceptionquery1 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'Firstname contains more than one name, ') WHERE firstname like '%&%' OR firstname like '% %' OR firstname like '% and %' OR firstname like '%/%' OR firstname like '%|%' OR firstname like '% or %' OR firstname like '%;%'");
$exceptionquery2 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'Lastname contains more than one name, ') WHERE lastname like '%&%' OR lastname like '% %' OR lastname like '% and %' OR lastname like '%/%' OR lastname like '%|%' OR lastname like '% or %' OR lastname like '%;%'");
$exceptionquery3 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'Email contains more than one value, ') WHERE email like '%&%' OR email like '% %' OR email like '% and %' OR email like '%/%' OR email like '%|%' OR email like '% or %' OR email like '%;%'");

// Email data validity
$exceptionquery4 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'Email address is invalid, ') WHERE `email` NOT REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$'");

// Empty fields
$exceptionquery5 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'No first name, ') WHERE firstname = '' OR firstname IS NULL");
$exceptionquery6 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'No last name, ') WHERE lastname = '' OR lastname IS NULL");
$exceptionquery7 = $db->query("UPDATE import SET issue = 1, details = CONCAT(details, 'No email, ') WHERE email = '' OR email IS NULL");

$qstring = '?status=clean';

//redirect to the previous page
header("Location: ../index.php".$qstring);

?>