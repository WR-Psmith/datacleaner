<?php
//include database configuration file
include '../config/dbConfig.php';

//get records from database
$query = $db->query("
SELECT * FROM import 
INNER JOIN (SELECT email
FROM import
GROUP BY email
HAVING COUNT(email) > 1) dup
ON import.email = dup.email
ORDER BY import.email Asc;
");

if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "voters_dupes_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Firstname', 'Lastname', 'Email');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['firstname'], $row['lastname'], $row['email']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>