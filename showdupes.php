<?php include('header.php'); 
//load the database configuration file
require base_path('/config/dbConfig.php');
?>
</head>

<body>
	<div class="container">
		<h1>Data cleaner - Dupes</h1>
		<?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Actions
			</div>
			<div class="panel-body">
				<input type="submit" class="btn btn-primary" name="back" value="Back" onClick="document.location.href='index.php'">
				
				<span style="float:right;">
					<input type="submit" class="btn btn-primary" name="Back" value="Export" onClick="document.location.href='dbfunctions/exportdupes.php'">
				</span>
			</div>
		</div>
		<?php $query = $db->query( "SELECT * FROM import 
							INNER JOIN (SELECT email
							FROM import
							GROUP BY email
							HAVING COUNT(email) > 1) dup
							ON import.email = dup.email
							ORDER BY import.email Asc;" ); ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Duplicate Records
				<span style="float: right;">Total records: <?php echo $query->num_rows; ?></span>
			</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Firstname</th>
							<th>Lastname</th>
							<th>Email</th>
							<th>Filename</th>
							<th>Actions</th>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						//get records from database
						if ( $query->num_rows > 0 ) {
							while ( $row = $query->fetch_assoc() ) {
								?>
						<tr>
							<td class="result">
								<?php echo $row['firstname']; ?>
							</td>
							<td class="result">
								<?php echo $row['lastname']; ?>
							</td>
							<td class="result">
								<?php echo $row['email']; ?>
							</td>
							<td class="result">
								<?php echo $row['filename']; ?>
							</td>
							<td class="result">
								<?php echo $row['actions']; ?>
							</td>
							<td class="contact-actions">
								<form action='dbfunctions/delete.php' method="post">
									<input type="hidden" name="delid" value="<?php echo $row['ID']; ?>">
									<input type="submit" class="btn btn-primary" name="submit" value="Delete">
								</form>
							
								<form action='update.php' method="post">
									<input type="hidden" name="updid" value="<?php echo $row['ID']; ?>">
									<input type="submit" class="btn btn-primary" name="submit" value="Update">
								</form>
							</td>
						</tr>
						<?php } }else{ ?>
						<tr>
							<td colspan="5">No record(s) found.....</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>

		</div>
<?php include('footer.php');