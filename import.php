<?php include('header.php') ?>

<body>
	<div class="container">
		<h1>Import Records</h1>
		<?php if(!empty($statusMsg)){
        echo '<div class="alert '.$statusMsgClass.'">'.$statusMsg.'</div>';
    } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				Actions
			</div>
			<div class="panel-body">
				<input type="submit" class="btn btn-primary" name="back" value="Back" onClick="document.location.href='index.php'">
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				Import CSV Files
			</div>
			<div class="panel-body">
				<form action="dbfunctions/importData.php" method="post" enctype="multipart/form-data" id="importFrm">
					<table>
						<tr>
							<td style="padding-right: 20px;"><input type="file" name="file"/></td>
							<td><span class="float: right;"><input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT"></span></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
<?php include('footer.php');